# README #

Ấn tượng ngay từ cái nhìn đầu tiên, dự án nhà phố Bảo Phú Residence nổi bật với kiến trúc châu Âu hiện đại cùng với vô số mảng xanh tươi tốt và không gian sông nước đem lại cảm giác yên bình, thư thái cho những chủ nhân tinh tương lai.

### Giới thiệu ###

Bảo Phú Residence là ứng dụng web nhúng để theo dõi toàn bộ thông tin về dự án Bảo Phú Residdence do Đại Phát Corporation phát triển.

Ứng dụng đang trong quá trình xây dựng và hoàn thiện, dự kiến sẽ tung bản ver-0.1 vào tháng 10/2021

### Chức năng ###

Theo dõi thông tin đự án
Cập nhật tiến độ dự án
Xem giỏ hàng
Nhúng kênh youtube để theo dõi video dự án

### Liên hệ ###
Địa chỉ: Võ Thị Liễu, An Phú Đông, Quận 12, Thành Phố Hồ Chí Minh
Email: daiphatcorpvn@gmail.com
Hotline: 07.888.777.95
Website: https://daiphat-corp.com/bao-phu-residence-q12/
